import threading
import ManagemantPools
import sys


sm1 =[]
con=threading.Semaphore(1)
is_finishing = False

def process(process_index):
    global is_finishing
    while is_finishing == False:
        name = threading.current_thread().getName()
        th_control = threading.Thread(target=control, args=(process_index,name,))
        th_control.start()

        sm1[process_index].acquire()
        if processManager[process_index].get_flag():
            th_delete = threading.Thread(target=delete, args=(process_index,name))
            th_delete.start()
            sm1[process_index].acquire()

def control(i,name):
    con.acquire()
    processManager[i].clear_request()
    processManager[i].requset_pools(_pools)
    processManager[i].allocate_pools()


    sm1[i].release()
    con.release()

def delete(i,name):
    con.acquire()
    processManager[i].clear_request()
    processManager[i].release_pools()
    sm1[i].release()
    con.release()

def show_information():
    global processManager
    global _available
    print("Pools: ")
    for i in range(10):
        print(_pools[i].get_num(), end=' - ')
    print("Available: ", _available)
    print("pro1:")
    processManager[0].print_pools()
    print("pro2:")
    processManager[1].print_pools()
    print("pro3:")
    processManager[2].print_pools()
    print("pro4:")
    processManager[3].print_pools()
    print("pro5:")
    processManager[4].print_pools()

def user_control():
    global is_finishing
    while True:
        key_input = input("")
        if key_input == '0':
            is_finishing = True
            sys.exit()
        if key_input == '1':
            show_information()

_pools = []
_available = []



for i in range(10):
    _pools.append(ManagemantPools.Pools("Pool_0" + str(i)))


for i in range(10):
    print(_pools[i].get_num(), end=' - ')
    _available.append(_pools[i].get_num())
ManagemantPools.set_available(_available)

processManager = []
for i in range(5):
    processManager.append(ManagemantPools.ManagementProcessPools(i))


th_control= threading.Thread(target=user_control)
th_control.start()


for i in range(5):
    sm1.append(threading.Semaphore(0))
    th = threading.Thread(target=process, args=(i,), name="thread_0"+str(i))
    th.start()
