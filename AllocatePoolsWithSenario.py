import threading
import ManagemantPools

def readfile(_namefile):
    li= []
    _lines = []
    _startFile = open(_namefile)
    for _line in _startFile:
        _lines.append(_line.replace('\n', ''))

    _templist = []
    for j in range(_lines.__len__()):
        _temp = _lines[j].split(' ')
        for i in range(1,_temp[1].__len__(),2):
            li.append(int(_temp[1][i]))
        _templist.append(_temp[0])
        _templist.append(li.copy())
        li.clear()
    all_process_pool =[[],[],[],[],[],[],[],[],[],[],[]]
    for z in range(0,_templist.__len__()-1):
        if _templist[z]=='P0-A':
            all_process_pool[0].append(_templist[z+1])
        elif _templist[z]=='P1-A':
            all_process_pool[1].append(_templist[z+1])
        elif _templist[z]=='P2-A':
            all_process_pool[2].append(_templist[z+1])
        elif _templist[z]=='P3-A':
            all_process_pool[3].append(_templist[z+1])
        elif _templist[z]=='P4-A':
            all_process_pool[4].append(_templist[z+1])
        elif _templist[z]=='P0-R':
            all_process_pool[5].append(_templist[z+1])
        elif _templist[z]=='P1-R':
            all_process_pool[6].append(_templist[z+1])
        elif _templist[z]=='P2-R':
            all_process_pool[7].append(_templist[z+1])
        elif _templist[z]=='P3-R':
            all_process_pool[8].append(_templist[z+1])
        elif _templist[z]=='P4-R':
            all_process_pool[9].append(_templist[z+1])
        elif _templist[z]=='Pool':
            all_process_pool[10].append(_templist[z+1])
    _templist.clear()
    return all_process_pool

def process(process_index):
    while all_process_pool[process_index+5].__len__()>0:
        name = threading.current_thread().getName()
        th_control = threading.Thread(target=control, args=(process_index,name,))
        th_control.start()

        sm1[process_index].acquire()
        if processManager[process_index].get_flag():
            th_delete = threading.Thread(target=delete, args=(process_index,name))
            th_delete.start()
            sm1[process_index].acquire()

def control(i,name):
    con.acquire()
    print(name, ": ")
    processManager[i].print_pools()
    _requset = all_process_pool[i].pop(0)
    processManager[i].allocate_pools(_requset)
    if processManager[i].get_flag()==False:
        all_process_pool[i].insert(0,_requset)
    processManager[i].print_pools()
    sm1[i].release()
    con.release()

def delete(i,name):
    con.acquire()
    processManager[i].release_pools(all_process_pool[i+5].pop(0))
    processManager[i].print_pools()
    sm1[i].release()
    con.release()


sel = input("Enter 1 or 2:")
if sel == '1':
    all_process_pool = readfile("Senariobibonbast.txt")
elif sel =='2':
    all_process_pool = readfile("Senariobabonbast.txt")

_pools = all_process_pool[10].pop(0)
ManagemantPools.set_available(_pools)
sm1 =[]
con=threading.Semaphore(1)


processManager = []
for i in range(5):
    processManager.append(ManagemantPools.ManagementProcessPools(i))


for i in range(5):
    sm1.append(threading.Semaphore(0))
    th = threading.Thread(target=process, args=(i,), name="thread_0"+str(i))
    th.start()


