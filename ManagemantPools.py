import random

_available = []

def set_available(__avail):
    global _available
    _available = __avail.copy()


class Pools:
    def __init__(self,name=""):
        self.name = name
        self.num = random.randint(1, 5)

    def get_name(self):
        return self.name

    def get_num(self):
        return self.num

    def set_name(self,name):
        self.name=name


class ManagementProcessPools:
    def __init__(self,process):
      self.process=process
      self.available_process = [0,0,0,0,0,0,0,0,0,0]
      self.request = []
      self.pre_request = []
      self.flag = False

    def clear_request(self):
        self.request.clear()
    def requset_pools(self,_pools):
        for i in range(_pools.__len__()):
            self.request.append(random.randint(0, _pools[i].get_num()))

    def allocate_pools(self,_request=0):
        global _available
        if self.pre_request.__len__() > 0:
            _request = self.pre_request.copy()
            self.pre_request.clear()
        else:
            if _request == 0:
                _request = self.request
        if self.compare(_available,_request) == False:
            self.pre_request = _request
            self.flag = False
        else:
            _available = list(map(int.__sub__, _available,_request)).copy()
            self.available_process = list(map(int.__add__, self.available_process,_request)).copy()
            self.flag = True

    def compare(self,list1,list2):
        for i in range(list1.__len__()):
            if list1[i] < list2[i]:
                return False
        return True

    def get_flag(self):
        return self.flag


    def requset_pools_release(self,_pools):
        for i in range(_pools.__len__()):
            self.request.append(random.randint(0, _pools[i]))

    def release_pools(self,_request=0):
        global _available
        if _request == 0:
            _request = self.request
            self.requset_pools_release(self.available_process)
        _available = list(map(int.__add__, _available,_request)).copy()
        self.available_process = list(map(int.__sub__, self.available_process,_request)).copy()

    def print_pools(self):
       print("available_process", self.available_process)
